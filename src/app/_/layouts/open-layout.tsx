import { OpenModalButton } from "@/features/create-post/public/open-modal-button";
import { UiFooter } from "@/shared/ui/ui-footer";
import { UiHeader } from "@/shared/ui/ui-header";

export async function OpenLayout({ children }: { children?: React.ReactNode }) {
  return (
    <div className="min-h-screen flex flex-col">
      <UiHeader title={"Simple Blog"} action={<OpenModalButton />} />
      <div className="container mx-auto flex items-center flex-col mb-10">
        <main className=" grow flex flex-col max-w-screen-xl w-full">
          {children}
        </main>
      </div>
      <UiFooter />
    </div>
  );
}
