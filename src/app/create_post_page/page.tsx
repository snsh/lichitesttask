import { Metadata } from 'next/types';
import { CreatePostPage } from '@/pages/create-post/public/create-post-page';

export const metadata: Metadata = {
  title: 'Simple Blog | Create Post',
  description: 'Simple Blog',
};

export default CreatePostPage;
