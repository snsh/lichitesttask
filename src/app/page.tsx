import { Metadata } from "next/types";
import { HomePage } from "@/pages/home/public/home-page";

export const metadata: Metadata = {
  title: "Simple Blog",
  description: "Simple Blog",
};

export default HomePage;
