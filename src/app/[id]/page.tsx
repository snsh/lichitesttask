import { PostDTO } from "@/entities/post/public";
import { getAllPosts, getPostById } from "@/entities/post/query";
import { PostPage } from "@/pages/post/public/post-page";

export async function generateStaticParams() {
  const posts: PostDTO[] = await getAllPosts();

  return posts.map((post) => ({
    slug: post.id.toString(),
  }));
}

export default PostPage;
