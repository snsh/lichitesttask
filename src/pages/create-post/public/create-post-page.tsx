import { PostCreateForm } from "@/features/create-post/public/post-create-form";
import React from "react";

export async function CreatePostPage() {
  return (
    <div className="flex w-full h-full justify-center items-center">
      <PostCreateForm />
    </div>
  );
}
