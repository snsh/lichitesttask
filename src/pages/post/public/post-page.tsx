import { getAllCommentsByPostId } from "@/entities/comment/query";
import { getPostById } from "@/entities/post/query";
import { CommentList } from "@/features/comment-list/public/comment-list";
import { CreateCommentForm } from "@/features/create-comment/public/create-comment-form";

type Props = {
  params: {
    id: string;
  };
};
export async function PostPage({ params: { id } }: Props) {
  const post = await getPostById(id);
  const comments = await getAllCommentsByPostId(id);

  return (
    <div className="h-full flex flex-col justify-between mx-4">
      <div>
        <h1 className="text-center text-2xl text-black bold-900 mb-4">
          {post.title}
        </h1>
        <p>{post.body}</p>
      </div>

      <div className="mt-20">
        <h2 className="text-xl text-black bold-900 mb-4">Комментарии</h2>
        <CreateCommentForm postId={+id} />
        <CommentList id={+id} comments={comments} />
      </div>
    </div>
  );
}
