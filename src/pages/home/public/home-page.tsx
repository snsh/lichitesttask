import { PostModal } from "@/entities/post/public/post-modal";
import { getAllPosts } from "@/entities/post/query";
import { PostList } from "@/features/post-list/public/post-list";
import React from "react";

export async function HomePage() {
  const posts = await getAllPosts();
  return (
    <div className="mx-4">
      <PostList posts={posts} />
      <PostModal />
    </div>
  );
}
