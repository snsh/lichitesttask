import { FC } from "react";

export const UiTextInput: FC<
  React.ComponentProps<"input"> & { label: string }
> = ({ label, ...rest }) => {
  return (
    <div className="mb-4">
      <label className="block mb-2" htmlFor={rest.id}>
        {label}
      </label>
      <input
        className="border border-gray-300 px-4 py-2 rounded-md w-full"
        {...rest}
      />
    </div>
  );
};
