import { FC } from "react";

export const UiForm: FC<React.ComponentProps<"form">> = ({
  children,
  ...props
}) => {
  return (
    <form className="px-4 py-2" {...props}>
      {children}
    </form>
  );
};
