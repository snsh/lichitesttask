import Link from 'next/link';

export const UiHeader: React.FC<{
  title: string;
  action: React.ReactNode;
}> = ({ title, action }) => {
  return (
    <header className="bg-gray-800 py-4 mb-5">
      <div className="container mx-auto flex items-center flex-col justify-around">
        <h1 className="text-white text-2xl font-bold mb-3">
          <Link href="/">{title}</Link>
        </h1>
        {action}
      </div>
    </header>
  );
};
