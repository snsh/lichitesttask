"use client";

export const UiModal: React.FC<{
  isOpen: boolean;
  setIsClose: () => void;
  content: React.ReactNode;
}> = ({ isOpen, content, setIsClose }) => {
  const closeModal = () => {
    setIsClose();
  };
  if (!isOpen) {
    return null;
  }
  return (
    <div
      className="fixed inset-0 bg-gray-500 bg-opacity-75 flex items-center justify-center"
      onClick={closeModal}
    >
      <div
        className="bg-white p-8 rounded max-w-2xl shadow-2xl "
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
        }}
      >
        {content}
      </div>
    </div>
  );
};
