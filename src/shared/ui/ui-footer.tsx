export function UiFooter(): React.ReactNode {
  return (
    <footer className="bg-gray-800 py-4 mt-auto min-h-24">
      <div className="container mx-auto text-white">
        <p>Контакты: example@example.com</p>
      </div>
    </footer>
  );
}
