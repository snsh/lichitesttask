import { FC } from "react";

export const UiTextArea: FC<
  React.ComponentProps<"textarea"> & { label: string }
> = ({ label, ...rest }) => {
  return (
    <div className="mb-4 w-full">
      <label className="block mb-2" htmlFor={rest.id}>
        {label}
      </label>
      <textarea
        className="border border-gray-300 px-4 py-2 rounded-md w-full"
        {...rest}
      />
    </div>
  );
};
