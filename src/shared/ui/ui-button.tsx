import React, { FC } from "react";

export const UiButton: FC<React.ComponentProps<"button">> = ({ ...props }) => {
  return (
    <button
      className={
        "bg-gray-500 hover:bg-gray-700 text-white font-semibold py-2 px-4 rounded"
      }
      {...props}
    >
      {props.children}
    </button>
  );
};
