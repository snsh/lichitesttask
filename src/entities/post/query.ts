"use server";
import { BASE_URL } from "@/shared/api";
import { revalidatePath } from "next/cache";
import { PostDTO } from "./model/types";

export async function getAllPosts(): Promise<PostDTO[]> {
  try {
    const response = await fetch(BASE_URL + `posts`);
    return response.json();
  } catch (error) {
    throw error;
  }
}

export async function getPostById(id: string): Promise<PostDTO> {
  try {
    const response = await fetch(BASE_URL + `posts/${id}`);
    return response.json();
  } catch (error) {
    throw error;
  }
}

export async function createPost({
  title,
  body,
}: {
  title: string;
  body: string;
}) {
  const response = await fetch(BASE_URL + `posts`, {
    method: "POST",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
    body: JSON.stringify({ userId: 1, title, body }),
  });

  const post = await response.json();
  revalidatePath("/");
  return post;
}
