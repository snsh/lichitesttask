'use client';
import { useAppDispatch, useAppSelector } from '@/shared/lib/redux';
import { PostModalStore } from '../model/post-modal-slice';
import { PostInSmallWindow } from '../ui/post-small-window';
import { UiModal } from '@/shared/ui/ui-modal';

export const PostModal: React.FC = () => {
  const dispatch = useAppDispatch();
  const isOpen = useAppSelector(PostModalStore.selectors.isOpen);
  const postData = useAppSelector(PostModalStore.selectors.getPostData);
  return (
    <UiModal
      isOpen={isOpen}
      setIsClose={() => dispatch(PostModalStore.actions.handleClose())}
      content={<PostInSmallWindow postData={postData} />}
    />
  );
};

export { PostModalStore };
