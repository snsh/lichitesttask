import React from 'react';

export type PostData = {
  title: string;
  description: string;
};
export const PostInSmallWindow = ({ postData }: { postData: PostData }) => {
  return (
    <div className="py-2 flex flex-col">
      <h2 className="text-xl font-semibold mb-3 text-center">{postData.title}</h2>
      <p> {postData.description}</p>
    </div>
  );
};
