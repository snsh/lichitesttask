"use client";
import {
  PayloadAction,
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import { Post, PostDTO } from "./types";
import { createBaseSelector, registerSlice } from "@/shared/lib/redux";
import { createPost } from "../query";

const initialState = {
  posts: [] as Post[],
};

const postListSlice = createSlice({
  name: "postList",
  initialState,
  reducers: {
    allPostsAdd(state, action: PayloadAction<PostDTO[]>) {
      state.posts = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(handleAdd.fulfilled, (state, action) => {
      state.posts = [action.payload, ...state.posts];
    });
  },
});

export const handleAdd = createAsyncThunk(
  "postList/handleAdd",
  async (data: { title: string; body: string }, { rejectWithValue }) => {
    try {
      console.log("tut2");
      const post: PostDTO = await createPost(data);

      return post;
    } catch (error: any) {
      return rejectWithValue(error);
    }
  }
);

const baseSelector = createBaseSelector(postListSlice);
registerSlice([postListSlice]);
export const PostListStore = {
  actions: {
    ...postListSlice.actions,
    handleAdd,
  },
  selectors: {
    getPosts: createSelector(baseSelector, (state) => state.posts),
  },
};
