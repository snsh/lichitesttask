export type Post = {
  id: number;
  title: string;
  body: string;
};

export type PostDTO = {
  userId: number;
  id: number;
  title: string;
  body: string;
};
