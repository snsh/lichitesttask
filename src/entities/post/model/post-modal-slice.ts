import { PostData } from '@/entities/post/ui/post-small-window';
import { createBaseSelector, registerSlice } from '@/shared/lib/redux';
import { createSelector, createSlice } from '@reduxjs/toolkit';

const initialState: { isOpen: boolean; postData: PostData } = {
  isOpen: false,
  postData: {} as PostData,
};

const PostModalSlice = createSlice({
  name: 'postModal',
  initialState,
  reducers: {
    handleOpen(state, action) {
      state.isOpen = true;
      state.postData = action.payload.postData;
    },
    handleClose(state) {
      state.isOpen = false;
      state.postData = {} as PostData;
    },
  },
});

const baseSelector = createBaseSelector(PostModalSlice);
registerSlice([PostModalSlice]);

export const PostModalStore = {
  actions: {
    ...PostModalSlice.actions,
  },
  selectors: {
    isOpen: createSelector(baseSelector, (state) => state.isOpen),
    getPostData: createSelector(baseSelector, (state) => state.postData),
  },
};
