"use client";
import {
  PayloadAction,
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import { createBaseSelector, registerSlice } from "@/shared/lib/redux";
import { CommentDTO } from ".";
import { createComment } from "../query";

const initialState = {
  comments: [] as CommentDTO[],
};

const commentsSlice = createSlice({
  name: "comments",
  initialState,
  reducers: {
    allCommentsAdd(state, action: PayloadAction<CommentDTO[]>) {
      state.comments = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(handleAdd.fulfilled, (state, action) => {
      state.comments = [action.payload, ...state.comments];
    });
  },
});

export const handleAdd = createAsyncThunk(
  "postList/handleAdd",
  async (
    data: { name: string; body: string; postId: number },
    { rejectWithValue }
  ) => {
    try {
      const comment: CommentDTO = await createComment(data);

      return { id: comment.id, ...data };
    } catch (error: any) {
      return rejectWithValue(error);
    }
  }
);

const baseSelector = createBaseSelector(commentsSlice);
registerSlice([commentsSlice]);
export const CommentsStore = {
  actions: {
    ...commentsSlice.actions,
    handleAdd,
  },
  selectors: {
    getComments: createSelector(baseSelector, (state) => state.comments),
  },
};
