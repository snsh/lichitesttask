export type CommentDTO = {
  postId: number;
  id: number;
  name: string;
  body: string;
};
