"use server";
import { revalidatePath } from "next/cache";
import { CommentDTO } from "./model";
import { BASE_URL } from "@/shared/api";

export async function getAllCommentsByPostId(
  id: string
): Promise<CommentDTO[]> {
  try {
    const response = await fetch(BASE_URL + `posts/${id}/comments`);
    return response.json();
  } catch (error) {
    throw error;
  }
}

export async function updateComment(data: CommentDTO, id: number) {
  try {
    const response = await fetch(
      BASE_URL + `posts/${data.postId.toString()}/comments/${id.toString()}}`,
      {
        method: "PATCH",
        body: JSON.stringify({ ...data, email: "@" }),
      }
    );
    console.log(response);
    revalidatePath(`/${data.postId}`);
  } catch (error) {
    throw error;
  }
}

export async function createComment(data: Omit<CommentDTO, "id">) {
  try {
    const response = await fetch(BASE_URL + `comments`, {
      method: "POST",
      body: JSON.stringify({ ...data, email: "@" }),
    });
    return response.json();
  } catch (error) {
    throw error;
  }
}
