"use client";
import { CommentsStore } from "@/entities/comment/model/comment-slice";
import { createComment } from "@/entities/comment/query";
import { useAppDispatch } from "@/shared/lib/redux";
import { UiButton } from "@/shared/ui/ui-button";
import { UiTextArea } from "@/shared/ui/ui-textarea";
import React, { useState } from "react";

export const CreateCommentForm: React.FC<{ postId: number }> = ({ postId }) => {
  const [value, setValue] = useState("");
  const dispatch = useAppDispatch();
  return (
    <div className="flex gap-2 w-full">
      <div className="grow">
        <UiTextArea
          label={"Оставьте комментарий"}
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
      </div>
      <div className="flex justify-center items-center">
        <UiButton
          onClick={() => {
            dispatch(
              CommentsStore.actions.handleAdd({
                name: "me",
                body: value,
                postId,
              })
            );
            setValue("");
          }}
        >
          Сохранить
        </UiButton>
      </div>
    </div>
  );
};
