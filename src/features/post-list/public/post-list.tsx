"use client";
import React, { useEffect } from "react";
import { PostCard } from "../ui/post-card";
import { getAllPosts } from "@/entities/post/query";
import { PostDTO } from "@/entities/post/public";
import { useAppDispatch, useAppSelector } from "@/shared/lib/redux";
import { PostListStore } from "@/entities/post/model/post-list-slice";

export const PostList: React.FC<{ posts: PostDTO[] }> = ({ posts }) => {
  const dispatch = useAppDispatch();
  const store_posts = useAppSelector(PostListStore.selectors.getPosts);
  useEffect(() => {
    dispatch(PostListStore.actions.allPostsAdd(posts));
  }, []);
  return (
    <ul className="container mx-auto grid grid-cols-1 md:grid-cols-3 gap-8">
      {store_posts.map((post) => (
        <li key={post.id}>
          <PostCard title={post.title} description={post.body} id={post.id} />
        </li>
      ))}
    </ul>
  );
};
