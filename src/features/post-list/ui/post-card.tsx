import Link from "next/link";
import { BookIconButton } from "./book-icon-button";

export const PostCard: React.FC<{
  id: number;
  title: string;
  description: string;
}> = ({ title, description, id }) => {
  return (
    <div className="bg-white p-4 shadow-md rounded-md h-full hover:shadow-xl">
      <Link href={`/${id}`}>
        <h2 className="text-xl font-semibold">{title}</h2>
      </Link>
      <p>{description.substring(0, 80) + `...`}</p>
      <div className="flex items-center mt-4">
        <span>
          <BookIconButton postData={{ title, description }} />
        </span>
      </div>
    </div>
  );
};
