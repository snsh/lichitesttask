import { UiButton } from '@/shared/ui/ui-button';
import Link from 'next/link';
import React from 'react';

export const OpenModalButton: React.FC = () => {
  return (
    <Link href="/create_post_page">
      <UiButton>Создать пост</UiButton>
    </Link>
  );
};
