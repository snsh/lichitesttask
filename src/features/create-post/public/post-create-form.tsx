"use client";
import { PostListStore } from "@/entities/post/model/post-list-slice";
import { useAppDispatch } from "@/shared/lib/redux";
import { UiButton } from "@/shared/ui/ui-button";
import { UiForm } from "@/shared/ui/ui-form";
import { UiTextInput } from "@/shared/ui/ui-text-input";
import { UiTextArea } from "@/shared/ui/ui-textarea";
import { useRouter } from "next/navigation";
import React, { FormEventHandler, useState } from "react";

export function PostCreateForm() {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const dispatch = useAppDispatch();
  const router = useRouter();
  const handleSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    try {
      e.preventDefault();
      dispatch(PostListStore.actions.handleAdd({ title, body }));
      router.push("/");
    } catch (error) {
      throw error;
    }
  };
  return (
    <UiForm onSubmit={handleSubmit}>
      <UiTextInput
        name="title"
        label="Название"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />
      <UiTextArea
        name="body"
        label="Содержание"
        value={body}
        onChange={(e) => setBody(e.target.value)}
      />
      <div className="flex justify-center">
        <UiButton type="submit">Создать</UiButton>
      </div>
    </UiForm>
  );
}
