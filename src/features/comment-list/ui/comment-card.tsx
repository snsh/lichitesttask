"use client";

import { updateComment } from "@/entities/comment/query";
import { useState } from "react";
import { ChangeIconButton } from "./change-button";
import { UiTextArea } from "@/shared/ui/ui-textarea";
import { UiButton } from "@/shared/ui/ui-button";

export const CommentCard: React.FC<{
  name: string;
  body: string;
  id: number;
  postId: number;
}> = ({ name, body, id, postId }) => {
  const [editMode, setEditMode] = useState(false);
  const [comment, setComment] = useState(body);
  const [isHover, setIsHover] = useState(false);

  const handleEdit = () => {
    setEditMode(!editMode);
  };

  const handleSave = async () => {
    await updateComment({ name, body: comment, id, postId }, id);
    setEditMode(false);
  };

  return (
    <div
      className="relative"
      onMouseEnter={() => {
        setIsHover(true);
      }}
      onMouseLeave={() => {
        setIsHover(false);
      }}
    >
      <div className="flex items-start">
        <div className="absolute top-0 right-0">
          {isHover && (
            <ChangeIconButton
              onClick={() => {
                handleEdit();
              }}
            />
          )}
        </div>
        <div className="grow">
          <h3 className="font-bold">{name}</h3>
          {editMode ? (
            <UiTextArea
              label="Изменить"
              value={comment}
              onChange={(e) => setComment(e.target.value)}
            />
          ) : (
            <p>{comment}</p>
          )}
        </div>
      </div>
      {editMode && <UiButton onClick={handleSave}>Сохранить</UiButton>}
    </div>
  );
};
