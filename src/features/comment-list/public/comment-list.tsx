"use client";
import React, { useEffect } from "react";
import { CommentCard } from "../ui/comment-card";
import { CommentDTO } from "@/entities/comment/model";
import { useAppDispatch, useAppSelector } from "@/shared/lib/redux";
import { CommentsStore } from "@/entities/comment/model/comment-slice";

export const CommentList: React.FC<{
  id: number;
  comments: CommentDTO[];
}> = ({ id, comments }) => {
  const dispatch = useAppDispatch();
  const store_comments = useAppSelector(CommentsStore.selectors.getComments);
  useEffect(() => {
    dispatch(CommentsStore.actions.allCommentsAdd(comments));
  }, []);
  return (
    <ul className="container mx-auto gap-8">
      {store_comments.map((comment) => (
        <li key={comment.id} className="my-4">
          <CommentCard
            name={comment.name}
            body={comment.body}
            id={comment.id}
            postId={id}
          />
        </li>
      ))}
    </ul>
  );
};
